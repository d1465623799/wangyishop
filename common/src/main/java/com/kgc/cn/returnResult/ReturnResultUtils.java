package com.kgc.cn.returnResult;


import com.kgc.cn.enums.EmployeeEnum;
import com.kgc.cn.enums.GoodsEnum;
import com.kgc.cn.enums.LoginEnum;
import com.kgc.cn.enums.PromotionEnum;

/***
 *
 * 统一返回工具类
 */
public class ReturnResultUtils {

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage("success");
        return returnResult;
    }

    /***
     * 成功 带信息
     * @return
     */
    public static ReturnResult returnSuccess(String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(message);
        return returnResult;
    }

    /***
     * 成功 带信息
     * @return
     */
    public static ReturnResult returnSuccess(LoginEnum loginEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(loginEnum.getMsg());
        return returnResult;
    }

    /***
     * 成功 带信息（商品）
     * @return
     */
    public static ReturnResult returnSuccess(GoodsEnum goodsEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(goodsEnum.getMsg());
        return returnResult;
    }

    /***
     * 成功 带信息（Employee）
     * @return
     */
    public static ReturnResult returnSuccess(EmployeeEnum employeeEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(employeeEnum.getMsg());
        return returnResult;
    }

    /***
     * 成功 带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 成功 带数据 带信息
     * @return
     */
    public static ReturnResult returnSuccess(Object data, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage(message);
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Integer code, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }

    /***
     * 失败使用枚举
     * @return
     */
    public static ReturnResult returnFail(LoginEnum loginEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(loginEnum.getCode());
        returnResult.setMessage(loginEnum.getMsg());
        return returnResult;
    }

    /***
     * 失败使用枚举
     * @return
     */
    public static ReturnResult returnFail(GoodsEnum GoodsEnum, String String) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(GoodsEnum.getCode());
        returnResult.setMessage(GoodsEnum.getMsg() + String);
        return returnResult;
    }

    /***
     * 失败使用枚举（Goods）
     * @return
     */
    public static ReturnResult returnFail(GoodsEnum goodsEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(goodsEnum.getCode());
        returnResult.setMessage(goodsEnum.getMsg());
        return returnResult;
    }

    /***
     * 失败使用枚举（Employee）
     * @return
     */
    public static ReturnResult returnFail(EmployeeEnum employeeEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(employeeEnum.getCode());
        returnResult.setMessage(employeeEnum.getMsg());
        return returnResult;
    }

    /***
     * 失败使用枚举（Employee）
     * @return
     */
    public static ReturnResult returnFail(EmployeeEnum employeeEnum, String str) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(employeeEnum.getCode());
        returnResult.setMessage(str);
        return returnResult;
    }

    /**
     * 失败使用枚举（promotion）
     */
    public static ReturnResult returnFail(PromotionEnum promotionEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(promotionEnum.getCode());
        returnResult.setMessage(promotionEnum.getMsg());
        return returnResult;
    }
    /**
     * 失败使用枚举（promotion）
     */
    public static ReturnResult returnFail(PromotionEnum promotionEnum,String string) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(promotionEnum.getCode());
        returnResult.setMessage(string);
        return returnResult;
    }


}
