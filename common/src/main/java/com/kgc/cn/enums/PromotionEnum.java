package com.kgc.cn.enums;

public enum PromotionEnum {
    VALUESUCCESS("更新成功"),
    ROLE_ERROR(410, "权限不足"),
    STARTTIME_ERROR(411, "开始时间不能小于当前时间"),
    PROMOTION(412,"该商品无促销活动或者不存在该商品"),
    ADD_ERROR(413, "添加失败"),
    DATE_FORMATE_ERROR(414, "日期格式不正确"),
    EXISTS_ERROR(415, "该折扣已存在"),
    NULL_VALUE_ERROR(416, "未输入商品ID"),
    DELETED_ERROR(417, "商品已下架"),
    DATE_ORDER_ERROR(418, "结束时间早于开始时间"),
    DATE_START_ERROR(419, "开始时间早于当前时间"),
    ENDTIME_ERROR(420,"结束时间不能小于开始时间"),
    UPDATE_ERROR(421,"修改失败，商品不存在"),
    PROMOTION_EMPTY(422,"无促销活动");

    int code;
    String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    PromotionEnum(String msg) {
        this.msg = msg;
    }

    PromotionEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
