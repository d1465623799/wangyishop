package com.kgc.cn.enums;

/**
 * Created by Ding on 2019/12/12.
 */
public enum LoginEnum {
    LOGIN_FAIL1(111, "手机号或密码错误!"),
    LOGIN_FAIL2(112, "该账号已离职!"),

    LOGINOUT_SUCCESS("成功注销登录！"),
    ROLE_ERROR(113, "权限不足"),
    ABSENTEEISM("暂无缺勤人员"),
    SEARCH_ERROR(100, "没有符合条件的员工"),
    DELETE_ERROR(101, "此员工不存在"),
    SEARCH_INPUT_ERROR(003, "请输入正确员工号"),
    UPDATE_ERROR(004, "修改失败"),
    PWD_INPUT_ERRPR(005,"未输入密码");

    int code;
    String msg;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    LoginEnum() {
    }

    LoginEnum(String msg) {
        this.msg = msg;
    }

    LoginEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
