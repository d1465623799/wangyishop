package com.kgc.cn.commonService;

import com.kgc.cn.model.dto.Goods;
import com.kgc.cn.model.dto.Goodsell;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ding on 2019/12/12.
 */
public interface GoodsConmonService {
    //查询售出商品总数量和总价格
    Goodsell querySell();

    // 通过excel表格批量加商品
    String addGoods(String filePath) throws IOException;

    // 查询商品名是否存在
    Goods goodNameisExist(String name);

    //查詢所有商品庫存
    List<Goods> queryGoodsNum(int current, int size);

    // 查询所有商品的数量
    int queryCount();

    // 根据商品id和商品名模糊查询商品
    List<Goods> queryGoodsInfo(int current, int size, String goodId, String goodName);

    //除了id，库存以为均可以修改
    boolean updateGoods(Goods goods);

    // 管理员权限根据商品ID下架商品
    int deleteGoodsByGoodIds(String goodId);

    // 查询查询出的商品总数
    int queryGoodCount(int current, int size, String goodId, String goodName);

    // 判断ID有无对应商品
    boolean hasGood(String goodId);
}
