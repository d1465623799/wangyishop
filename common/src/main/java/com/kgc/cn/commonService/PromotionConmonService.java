package com.kgc.cn.commonService;

import com.kgc.cn.model.dto.Promotion;

import java.util.List;

/**
 * 4.
 * Created by Ding on 2019/12/12.
 */
public interface PromotionConmonService {
    // 通过商品id修改促销活动
    String updatePromotion(Promotion promotion) throws Exception;

    //通过商品id删除促销活动
    int deletePromotion(String goodId);

    // 通过商品id精确查询促销活动
    Promotion queryPromotion(String goodId) throws Exception;

    //促销活动分页查询
    List<Promotion> promotionPage(int current, int size);

    // 增加促销活动
    int addPromotion(Promotion promotion);

    // 判断有没有这个折扣
    boolean hasPromorion(String goodId);

    // 判断商品有没有下架
    boolean isDelete(String goodId);

    //查询促销活动总数量
    int queryCount();

    // 更新促销活动
    int updateByPrimaryKey(Promotion promotion);
}
