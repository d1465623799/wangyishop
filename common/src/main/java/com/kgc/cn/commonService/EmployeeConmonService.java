package com.kgc.cn.commonService;

import com.kgc.cn.model.dto.Employee;

import java.util.List;

/**
 * Created by Ding on 2019/12/12.
 */
public interface EmployeeConmonService {
    //查询id对应的权限
    String queryRoleName(int roleId);

    //查询手机号对应员工
    Employee queryEmployee(String phone);

    //查询身份证号码对应的员工
    Employee queryEmployeeByIdCard(String IdCard);

    //查询缺勤人员
    List<Employee> queryAbsenteeism(int current, int size);

    // 通过excel批量添加员工
    String addEmployee(String filePath) throws Exception;

    // 根据工号姓名模糊条件查询，可用起止时间缩小查询范围
    List<Employee> queryEmployeeInfo(int current, int size, List<String> list);

    // 根据员工号删除员工（逻辑删除）
    int deleteEmployee(String employeeId);

    // 管理员更新信息
    int updateInfos(Employee employee);

    //查询出勤表数据总数量
    int queryCount();

    // 查询职员总数
    int queryEmpCount(int current, int size, List<String> list);

    // 修改个人密码
    boolean updatePassord(String employeeId, String password);
}
