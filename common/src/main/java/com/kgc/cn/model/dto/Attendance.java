package com.kgc.cn.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 出勤表
 */
@Data
@Builder
public class Attendance implements Serializable {
    private static final long serialVersionUID = 3995611317709917203L;
    //员工id
    private String empId;
    //出勤时间
    private String workTime;
}