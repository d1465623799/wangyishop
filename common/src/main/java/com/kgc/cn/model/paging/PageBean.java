package com.kgc.cn.model.paging;

import java.util.List;

/**
 * Created by Ding on 2019/12/16.
 */
public class PageBean<T> {

    //属性数据从数据库中查询
    //这一页数据
    private List<T> data;
    //总条数
    private int count;
    //值由客户端页面提供
    // 当前页
    private int current;
    //每页条数
    private int size;
    //由其他的属性计算出来的
    // 总页数
    private int total;
    //首页
    private int first;
    //上页
    private int previous;
    //下页
    private int next;

    //注：所有的计算方法都写在get方法中，调用get方法
    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    //计算总页面
    public int getTotal() {
        //如果总条数能够整除页大小，正好是这么多页，如果不能整除，页数加1
        return getCount() % getSize() == 0 ? getCount() / getSize() : getCount() / getSize() + 1;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    //第一页
    public int getFirst() {
        return 1;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    //得到上一页
    public int getPrevious() {
        //当前页减1
        if (getCurrent() > 1) {
            return getCurrent() - 1;
        } else {
            return 1;
        }
    }

    public void setPrevious(int previous) {
        this.previous = previous;
    }

    //得到下一页
    public int getNext() {
        //如果当前页小于最后一页
        if (getCurrent() < getTotal()) {
            return getCurrent() + 1;
        } else {
            //返回最后一页
            return getTotal();
        }
    }

    public void setNext(int next) {
        this.next = next;
    }

}