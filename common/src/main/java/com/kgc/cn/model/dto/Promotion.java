package com.kgc.cn.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class Promotion implements Serializable {
    private static final long serialVersionUID = 3340361423503069315L;
    //商品id
    private String goodId;
    //促销开始时间
    private String timeStart;
    //促销结束时间
    private String timeEnd;
    //折扣力度：10为一折
    private Integer discount;
    //商品idSet
    private Set<String> goodIdSet;
    //商品名称
    private String goodName;
}