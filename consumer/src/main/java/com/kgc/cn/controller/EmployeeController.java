package com.kgc.cn.controller;

import com.kgc.cn.AsayncClass.AsyncTask;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.consumerService.EmployeeConsumerService;
import com.kgc.cn.enums.EmployeeEnum;
import com.kgc.cn.enums.LoginEnum;
import com.kgc.cn.model.*;
import com.kgc.cn.returnResult.ReturnResult;
import com.kgc.cn.returnResult.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.concurrent.Future;

/**
 * Created by Ding on 2019/12/12.
 */
@RestController
@Api(tags = "员工")
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeConsumerService employeeService;

    /**
     * 用户登录
     *
     * @param phone
     * @param password
     * @param request
     * @return
     */
    @ApiOperation(value = "登录")
    @PostMapping(value = "/login")
    public ReturnResult login(@ApiParam("手机号") @RequestParam String phone,
                              @ApiParam("密码") @RequestParam String password,
                              HttpServletRequest request) {
        String sessionId = null;
        try {
            sessionId = employeeService.login(phone, password, request);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (sessionId.equals("1")) {
            return ReturnResultUtils.returnFail(LoginEnum.LOGIN_FAIL1);
        }
        if (sessionId.equals("2")) {
            return ReturnResultUtils.returnFail(LoginEnum.LOGIN_FAIL2);
        }
        return ReturnResultUtils.returnSuccess(sessionId);
    }

    /**
     * 注销登录
     *
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "注销登录")
    @PostMapping(value = "/loginOut")
    public ReturnResult loginOut(@CurrentUser EmployeeParam employeeParam, HttpServletRequest request) {
        // 注销登录
        employeeService.loginOut(request);
        return ReturnResultUtils.returnSuccess(LoginEnum.LOGINOUT_SUCCESS);
    }

    /**
     * 查询缺勤员工
     *
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "查询缺勤员工")
    @GetMapping(value = "/queryAttendance")
    public ReturnResult queryAttendance(@ApiParam(value = "当前页") @RequestParam(defaultValue = "1") String current,
                                        @ApiParam(value = "每页大小") @RequestParam(defaultValue = "5") String size) {
        int currentInt = Integer.parseInt(current);
        int sizeInt = Integer.parseInt(size);
        PageBeanParam<AttendanceParam> pageBeanParam = employeeService.queryAbsenteeism(currentInt, sizeInt);
        if (null == pageBeanParam) {
            return ReturnResultUtils.returnSuccess(LoginEnum.ABSENTEEISM);
        }
        return ReturnResultUtils.returnSuccess(pageBeanParam);
    }

    @Autowired
    private AsyncTask asyncTask;

    /**
     * 登录状态根据权限来使用excel表格批量添加员工
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    @LoginRequired(roleId = 2)
    @ApiOperation(value = "增加员工")
    @PostMapping(value = "/addEmployee")
    public ReturnResult addEmployee(@ApiParam(value = "文件地址") @RequestParam String filePath) throws Exception {
        String isTrue = null;
        // 调用方法
        if (!StringUtils.isEmpty(filePath)) {
            Future<String> future = asyncTask.addEmployee(filePath);
            while (true) {
                if (future.isDone()) {
                    isTrue = future.get();
                    break;
                }
            }
            if (filePath.length() == 0) {
                return ReturnResultUtils.returnFail(EmployeeEnum.EMP_NOFILR);
            } else if (isTrue.contains("203") && isTrue.contains("204")) {
                return ReturnResultUtils.returnFail(EmployeeEnum.EMP_ADDRESTARTANDRESTART, isTrue.replace("203", "").replace("204", ""));
            } else if (isTrue.contains("203")) {
                return ReturnResultUtils.returnFail(EmployeeEnum.EMP_ADDREPEAT, isTrue.replace("203", ""));
            } else if (isTrue.contains("204")) {
                return ReturnResultUtils.returnFail(EmployeeEnum.EMP_ADDRESTART, isTrue.replace("204", ""));
            } else {
                return ReturnResultUtils.returnSuccess(EmployeeEnum.EMP_ADDSUCCESS, isTrue);
            }
        } else {
            return ReturnResultUtils.returnFail(EmployeeEnum.EMP_ADDNULL);
        }

    }

    /**
     * 显示个人信息
     *
     * @param employeeParam
     * @return
     */
    @ApiOperation(value = "显示个人信息")
    @PostMapping(value = "/showInfo")
    @LoginRequired
    public ReturnResult showInfo(@CurrentUser EmployeeParam employeeParam) {
        SearchEmployeeParam searchEmployeeParam = new SearchEmployeeParam();

        searchEmployeeParam.setId(employeeParam.getEmpId());
        searchEmployeeParam.setName(employeeParam.getEmpName());
        return ReturnResultUtils.returnSuccess(employeeService.queryEmployeeInfo(1, 1, searchEmployeeParam));


    }

    /**
     * 根据登陆者权限查询库中信息
     *
     * @param searchEmployeeParam
     * @return
     */
    @ApiOperation(value = "根据条件查询员工")
    @PostMapping(value = "/queryInfo")
    @LoginRequired(roleId = 1)
    public ReturnResult queryInfo(@ApiParam(value = "当前页") @RequestParam(defaultValue = "1") String current,
                                  @ApiParam(value = "每页大小") @RequestParam(defaultValue = "5") String size,
                                  @Valid SearchEmployeeParam searchEmployeeParam) {
        int currentInt = Integer.parseInt(current);
        int sizeInt = Integer.parseInt(size);
        // 接收查询结果
        PageBeanParam<EmployeeMsgParam> pageBeanParam = employeeService.queryEmployeeInfo(currentInt, sizeInt, searchEmployeeParam);
        // 判断集合是否为空
        if (!CollectionUtils.isEmpty(pageBeanParam.getData())) {
            return ReturnResultUtils.returnSuccess(pageBeanParam);
        } else {
            return ReturnResultUtils.returnFail(LoginEnum.SEARCH_ERROR);
        }
    }

    /**
     * 逻辑删除员工
     *
     * @param employeeId
     * @param employeeParam
     * @return
     */
    @ApiOperation(value = "离职员工")
    @PostMapping(value = "/deleteEmployee")
    @LoginRequired(roleId = 1)
    public ReturnResult deleteEmployee(@ApiParam("要离职的员工ID") @RequestParam String employeeId,
                                       @CurrentUser EmployeeParam employeeParam) {
        // 判断输入的ID是不是本人的ID
        if (employeeId.equals(employeeParam.getEmpId())) {
            return ReturnResultUtils.returnFail(LoginEnum.SEARCH_INPUT_ERROR);
        }
        // 判断是否删除成功
        if (employeeService.deleteEmployee(employeeId) != 1) {
            return ReturnResultUtils.returnFail(LoginEnum.DELETE_ERROR);
        }
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 修改员工信息
     *
     * @param manageParam
     * @param currentParam
     * @return
     */
    @ApiOperation(value = "修改员工信息")
    @PostMapping(value = "/updateEmployee")
    @LoginRequired
    public ReturnResult updateEmployee(@Valid ManageParam manageParam,
                                       @CurrentUser EmployeeParam currentParam) {
        // 判断用户权限
        String empRole = employeeService.queryRoleName(currentParam.getRoleId());
        // 如果不是管理员，就将现在登录的员工ID填入employeeParam传走然后将其不能修改的值清空
        if (!"管理员".equals(empRole)) {
            // 判断ID是否输入正确，非管理员只能输入自己ID
            if (!manageParam.getEmpId().equals(currentParam.getEmpId())) {
                return ReturnResultUtils.returnFail(LoginEnum.SEARCH_INPUT_ERROR);
            } else {
                // 向后面传值
                manageParam.setEmpId(currentParam.getEmpId());
                NormalEmpParam normalEmpParam = new NormalEmpParam();
                BeanUtils.copyProperties(manageParam, normalEmpParam);
                // 判断非管理员执行结果
                int result = employeeService.updateInfo(normalEmpParam);
                // 成功
                return ReturnResultUtils.returnSuccess();
            }
        } else {
            // 如果是管理员，这边输入的ID就是需要修改的员工的ID，不能是自己ID
            if (manageParam.getEmpId().equals(currentParam.getEmpId())) {
                return ReturnResultUtils.returnFail(LoginEnum.SEARCH_INPUT_ERROR);
            } else {
                // 判断管理员执行结果
                int result = employeeService.updateInfos(manageParam);
                // 成功
                return ReturnResultUtils.returnSuccess();
            }
        }
    }

    /**
     * 修改个人密码
     * @param employeeParam
     * @param password
     * @param request
     * @return
     */
    @ApiOperation(value = "修改个人密码")
    @PostMapping(value = "/updatePassword")
    @LoginRequired
    public ReturnResult updatePassword(@CurrentUser EmployeeParam employeeParam,
                                       @ApiParam(value = "新密码") @RequestParam String password, HttpServletRequest request) {
        if (StringUtils.isEmpty(password)) {
            return ReturnResultUtils.returnFail(EmployeeEnum.EMP_UPDPWDNULL);
        } else {
            boolean isOk = employeeService.updatePassord(employeeParam.getEmpId(), password, request);
            if (isOk) {
                return ReturnResultUtils.returnSuccess(EmployeeEnum.EMP_UPDPWDSUCCESS);
            }
            return ReturnResultUtils.returnFail(EmployeeEnum.EMP_UPDPWDFAIL);
        }


    }
}
