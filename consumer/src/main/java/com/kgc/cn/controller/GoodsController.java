package com.kgc.cn.controller;

import com.kgc.cn.AsayncClass.AsyncTask;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.consumerService.EmployeeConsumerService;
import com.kgc.cn.consumerService.GoodsConsumerService;
import com.kgc.cn.enums.GoodsEnum;
import com.kgc.cn.model.EmployeeParam;
import com.kgc.cn.model.GoodsMsgParam;
import com.kgc.cn.model.GoodsUpdateParam;
import com.kgc.cn.model.PageBeanParam;
import com.kgc.cn.returnResult.ReturnResult;
import com.kgc.cn.returnResult.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.Future;

/**
 * Created by Ding on 2019/12/12.
 */
@RestController
@Api(tags = "商品")
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsConsumerService goodsService;
    @Autowired
    private EmployeeConsumerService employeeService;
    @Autowired
    private AsyncTask asyncTask;

    /**
     * 通过excel表格批量添加商品
     *
     * @param filePath
     * @return
     * @throws IOException
     */
    @LoginRequired(roleId = 2)
    @ApiOperation(value = "通过excel表格添加商品")
    @PostMapping(value = "/addGoods")
    public ReturnResult addGoods(@ApiParam(value = "文件地址") @RequestParam String filePath) throws Exception {
        String flag = null;
        if (!StringUtils.isEmpty(filePath)) {
            Future<String> future = asyncTask.addGoods(filePath);
            while (true) {
                if (future.isDone()) {
                    flag = future.get();
                    break;
                }
            }
            if ("0".equals(flag)) {
                // 书籍添加成功
                return ReturnResultUtils.returnSuccess(GoodsEnum.GOODS_ADDSUCESS);
            } else {
                return ReturnResultUtils.returnFail(GoodsEnum.GOODS_ADDFAILTURE);
            }
        } else {
            return ReturnResultUtils.returnFail(GoodsEnum.GOODS_ADDNULL);
        }

    }


    /**
     * 查询售出商品总数量和总价格
     *
     * @return
     */
    @LoginRequired(roleId = 1)
    @ApiOperation(value = "查询卖出的商品")
    @GetMapping(value = "/queryGoodsSell")
    public ReturnResult queryGoodsSell() {
        return ReturnResultUtils.returnSuccess(goodsService.querySell());
    }

    /**
     * 按商品类型返回所有商品的庫存
     *
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "按商品类型返回所有商品的庫存")
    @GetMapping(value = "/queryGoodsNum")
    public ReturnResult queryGoodsNum(@ApiParam(value = "当前页") @RequestParam(defaultValue = "1") String current,
                                      @ApiParam(value = "每页条数") @RequestParam(defaultValue = "5") String size) {
        int currentInt = Integer.parseInt(current);
        int sizeInt = Integer.parseInt(size);
        return ReturnResultUtils.returnSuccess(goodsService.queryGoodsNum(currentInt, sizeInt));
    }

    /**
     * 根据商品id和商品名模糊查询商品
     *
     * @param goodsId
     * @param goodsName
     * @return
     */
    @ApiOperation(value = "查询商品")
    @PostMapping(value = "/queryGoods")
    public ReturnResult queryGoods(@ApiParam(value = "当前页") @RequestParam(defaultValue = "1") String current,
                                   @ApiParam(value = "每页大小") @RequestParam(defaultValue = "5") String size,
                                   @ApiParam(value = "商品ID") @RequestParam String goodsId,
                                   @ApiParam(value = "商品名字") @RequestParam String goodsName) {
        int currentInt = Integer.parseInt(current);
        int sizeInt = Integer.parseInt(size);
        PageBeanParam<GoodsMsgParam> msgParamPageBeanParam = goodsService.queryGoodsInfo(currentInt, sizeInt, goodsId, goodsName);
        if (CollectionUtils.isEmpty(msgParamPageBeanParam.getData())) {
            return ReturnResultUtils.returnFail(GoodsEnum.GOODS_SEARCH_ERROR);
        }
        return ReturnResultUtils.returnSuccess(msgParamPageBeanParam);
    }

    /**
     * 权限：管理员
     * 除了id，库存以外均可以修改
     *LoginRequired  roleId = 1 管理员权限
     * @param employeeParam
     * @param goodsUpdateParam
     * @return
     */
    @LoginRequired(roleId = 1)
    @ApiOperation(value = "修改商品信息")
    @PostMapping(value = "/updateGoods")
    public ReturnResult updateGoods(@CurrentUser EmployeeParam employeeParam, @Valid GoodsUpdateParam goodsUpdateParam) {
            String isOk = goodsService.updateGoods(goodsUpdateParam);
            if ("1".equals(isOk)) {
                return ReturnResultUtils.returnSuccess(GoodsEnum.GOODS_UPDATESUCCESS);
            } else if ("0".equals(isOk)) {
                return ReturnResultUtils.returnFail(GoodsEnum.GOODS_NULL);
            } else {
                return ReturnResultUtils.returnFail(GoodsEnum.GOODS_EMPTY);
            }
    }

    /**
     * 商品促销活动下架（管理员权限）
     *
     * @param goodIdSet
     * @return
     */
    @ApiOperation(value = "商品下架")
    @PostMapping(value = "/deleteGoods")
    @LoginRequired(roleId = 1)
    public ReturnResult deleteGoods(@ApiParam("欲下架商品ID") @RequestParam Set<String> goodIdSet) {
        // 判断执行结果
        String result = goodsService.deleteGoodsByGoodIds(goodIdSet);
        if (result.equals("200")) return ReturnResultUtils.returnFail(GoodsEnum.NULL_VALUE_ERROR);

        return ReturnResultUtils.returnSuccess(result);
    }

}
