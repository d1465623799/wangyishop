package com.kgc.cn.consumerService.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kgc.cn.commonService.GoodsConmonService;
import com.kgc.cn.commonService.PromotionConmonService;
import com.kgc.cn.consumerService.GoodsConsumerService;
import com.kgc.cn.model.*;
import com.kgc.cn.model.dto.Goods;
import com.kgc.cn.model.dto.Goodsell;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ding on 2019/12/12.
 */
@Service
public class GoodsConsumerImpl implements GoodsConsumerService {
    @Reference(timeout = 3000)
    private GoodsConmonService goodsService;

    @Autowired
    private EmployeeConsumerImpl employeeConsumer;

    @Reference
    private PromotionConmonService promotionConmonService;

    /**
     * 通过excel表格批量添加商品
     *
     * @param filePath
     * @return
     * @throws IOException
     */

    @Override
    public String addGoods(String filePath) throws IOException {
        return goodsService.addGoods(filePath);

    }


    /**
     * 查询售出商品总数量和总价格
     *
     * @return
     */
    @Override
    public SellParam querySell() {
        Goodsell goodsell = goodsService.querySell();
        SellParam sellParam = new SellParam();
        BeanUtils.copyProperties(goodsell, sellParam);
        return sellParam;
    }

    /**
     * 查詢所有商品的庫存(根據商品類型返回)
     *
     * @return
     */
    @Override
    public PageBeanParam queryGoodsNum(int current, int size) {
        // list裏面存著一页所有信息
        List<Goods> list = goodsService.queryGoodsNum(current, size);
        Map<String, List<GoodsNumParam>> map = Maps.newHashMap();
        // 这一页第一个商品的商品类型
        int firsttypeid = list.get(0).getTypeId();
        // 遍历商品类型
        for (int i = firsttypeid; ; i++) {
            List<GoodsNumParam> listparam = Lists.newArrayList();
            // 遍历list
            for (Goods goods : list) {
                if (goods.getTypeId() == i) {
                    // 从list里面拿取自己需要的信息
                    GoodsNumParam goodsNumParam = GoodsNumParam.builder()
                            .goodName(goods.getGoodName())
                            .goodNum(goods.getGoodNum())
                            .typeName(goods.getGoodsType().getTypeName())
                            .build();
                    listparam.add(goodsNumParam);
                }
            }
            // 当listparam为空，说明无此商品类型
            if (CollectionUtils.isEmpty(listparam)) {
                break;
            }
            // 放入map
            map.put(listparam.get(0).getTypeName(), listparam);
        }
        // 放入list用来保存到pagebean里面进行分页
        List<Map<String, List<GoodsNumParam>>> mapList = Lists.newArrayList();
        // 用于分页
        mapList.add(map);
        PageBeanParam<Map<String, List<GoodsNumParam>>> pageBeanParam = PageBeanParam.<Map<String, List<GoodsNumParam>>>builder()
                .current(current)
                .size(size)
                .count(goodsService.queryCount())
                .data(mapList)
                .build();
        return pageBeanParam;
    }

    /**
     * 通过商品ID和商品名模糊查询商品，如果条件为空，则返回所有商品。
     *
     * @param goodsId
     * @param goodsName
     * @return
     */
    @Override
    public PageBeanParam<GoodsMsgParam> queryGoodsInfo(int current, int size, String goodsId, String goodsName) {
        // 新建集合接收数据库查询结果
        List<Goods> goodsList = goodsService.queryGoodsInfo(current, size, goodsId, goodsName);
        List<GoodsMsgParam> goodsMsgParamList = Lists.newArrayList();
        // 循环遍历转换
        goodsList.forEach(goods -> {
            GoodsMsgParam goodsMsgParam = new GoodsMsgParam();
            BeanUtils.copyProperties(goods, goodsMsgParam);
            // 判断并用具体属于替换代号
            if (goods.getIsDelete() == 1) {
                goodsMsgParam.setIsDeleteMsg("在售");
            } else {
                goodsMsgParam.setIsDeleteMsg("已下架");
            }
            goodsMsgParamList.add(goodsMsgParam);
        });
        // 赋值
        PageBeanParam<GoodsMsgParam> pageBeanParam = PageBeanParam.<GoodsMsgParam>builder()
                .current(current)
                .size(size)
                .count(goodsService.queryGoodCount(current, size, goodsId, goodsName))
                .data(goodsMsgParamList)
                .build();
        return pageBeanParam;
    }

    /**
     * 更新商品信息
     *
     * @param goodsUpdateParam
     * @return
     */
    @Override
    public String updateGoods(GoodsUpdateParam goodsUpdateParam) {

        if (StringUtils.isEmpty(goodsUpdateParam.getGoodContent()) && StringUtils.isEmpty(goodsUpdateParam.getGoodName())
                && StringUtils.isEmpty(goodsUpdateParam.getGoodPrice()) && StringUtils.isEmpty(goodsUpdateParam.getPictureSource())
                && StringUtils.isEmpty(goodsUpdateParam.getTypeId()) && StringUtils.isEmpty(goodsUpdateParam.getIsDelete())) {
            return "0";
        } else {
            Goods goods = new Goods();
            BeanUtils.copyProperties(goodsUpdateParam, goods);
            boolean flag = goodsService.updateGoods(goods);
            if (flag) {
                return "1";
            } else {
                return "2";
            }

        }
    }

    /**
     * 管理员权限根据商品ID下架商品
     *
     * @param goodIdSet
     * @return
     */
    @Transactional
    @Override
    public String deleteGoodsByGoodIds(Set<String> goodIdSet) {
        // 判断传入商品ID是否为空
        if (goodIdSet.size() == 0 && goodIdSet.isEmpty()) return "200";

        StringBuffer exists = new StringBuffer();
        // 循环遍历执行下架商品并删除对应促销
        goodIdSet.forEach(goodIdStr -> {
            if (!goodsService.hasGood(goodIdStr)) {
                exists.append(goodIdStr + "，");
            } else {
                goodsService.deleteGoodsByGoodIds(goodIdStr);
                promotionConmonService.deletePromotion(goodIdStr);
            }
        });
        if ("".equals(exists.toString())) {
            return "成功~~~";
        } else {
            return "以下商品不存在：" + exists;
        }
    }

}
