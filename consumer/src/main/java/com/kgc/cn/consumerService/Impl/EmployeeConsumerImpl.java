package com.kgc.cn.consumerService.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.kgc.cn.commonService.AttendanceConmonService;
import com.kgc.cn.commonService.EmployeeConmonService;
import com.kgc.cn.consumerService.EmployeeConsumerService;
import com.kgc.cn.model.*;
import com.kgc.cn.model.dto.Attendance;
import com.kgc.cn.model.dto.Employee;
import com.kgc.cn.utils.DES.DESUtil;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

/**
 * Created by Ding on 2019/12/12.
 */
@Service
public class EmployeeConsumerImpl implements EmployeeConsumerService {
    @Reference(timeout = 30000)
    private EmployeeConmonService employeeService;
    @Reference
    private AttendanceConmonService attendanceService;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 登录
     *
     * @param phone
     * @param password
     * @param request
     * @return
     */
    @Override
    public String login(String phone, String password, HttpServletRequest request) throws ParseException {
        Employee employee = employeeService.queryEmployee(phone);
        if (null != employee) {
            if (employee.getIsDelete() == 0) {
                String passwordDES = DESUtil.getDecryptString(employee.getEmpPassword());
                if (passwordDES.equals(password)) {
                    String sessionId = request.getSession().getId();
                    String employeeStr = JSONObject.toJSONString(employee);
                    //将员工信息存到redis中，并设置缓存时间
                    redisUtils.set(sessionId, employeeStr, 60 * 60);
                    // 查询有没有出勤记录，如果有则更新时间，没有则添加一条出勤记录
                    Attendance attendance = attendanceService.queryAttendance(employee.getEmpId());
                    if (null == attendance) {
                        attendanceService.insertAttendance(employee.getEmpId());
                    } else {
                        attendanceService.updateWorkTime(employee.getEmpId());
                    }
                    return sessionId;
                }
                return "1";
            }
            return "2";
        }
        return "1";
    }

    /**
     * 注销登录
     *
     * @return
     */
    @Override
    public void loginOut(HttpServletRequest request) {
        String str = request.getHeader("token");
        redisUtils.del(str);
    }


    /**
     * 查询未出勤员工
     *
     * @return
     */
    @Override
    public PageBeanParam<AttendanceParam> queryAbsenteeism(int current, int size) {

        //从数据库查询数据
        List<Employee> employeeList = employeeService.queryAbsenteeism(current, size);
        if (CollectionUtils.isEmpty(employeeList)) {
            return null;
        } else {
            List<AttendanceParam> attendanceParamList = Lists.newArrayList();
            employeeList.forEach(employee -> {
                AttendanceParam attendanceParam = new AttendanceParam();
                BeanUtils.copyProperties(employee, attendanceParam);
                attendanceParamList.add(attendanceParam);
            });
            PageBeanParam<AttendanceParam> pageBeanParam = PageBeanParam.<AttendanceParam>builder()
                    .current(current)
                    .size(size)
                    .count(queryCount())
                    .data(attendanceParamList)
                    .build();
            return pageBeanParam;
        }
    }

    /**
     * 查询未出勤数据总数量
     * 用于分页
     *
     * @return
     */
    private int queryCount() {
        return employeeService.queryCount();
    }

    /**
     * 查询员工权限
     *
     * @param roleId
     * @return
     */
    @Override
    public String queryRoleName(int roleId) {
        return employeeService.queryRoleName(roleId);
    }

    /**
     * 通过excel批量添加员工
     *
     * @return
     */
    @Override
    public String addEmployee(String filePath) throws Exception {
        return employeeService.addEmployee(filePath);
    }

    /**
     * 模糊查询员工信息
     * 可以用起止时间缩小范围
     *
     * @param searchEmployeeParam
     * @return
     */
    @Override
    public PageBeanParam<EmployeeMsgParam> queryEmployeeInfo(int current, int size, SearchEmployeeParam searchEmployeeParam) {
        // 用list存放前端传值
        List<String> conditionList = Lists.newArrayList();
        conditionList.add(searchEmployeeParam.getId());
        conditionList.add(searchEmployeeParam.getName());
        conditionList.add(searchEmployeeParam.getTimeStart());
        conditionList.add(searchEmployeeParam.getTimeEnd());
        // 新建前台返回类的集合
        List<EmployeeMsgParam> employeeMsgParamList = Lists.newArrayList();
        // 新建集合接收数据库的查询结果
        List<Employee> employeeList = employeeService.queryEmployeeInfo(current, size, conditionList);
        // 循环遍历转换
        employeeList.forEach(employee -> {
            EmployeeMsgParam employeeMsgParam = new EmployeeMsgParam();
            BeanUtils.copyProperties(employee, employeeMsgParam);
            // 判断并将性别代号转换为性别
            if (employee.getEmpSex() == 1) {
                employeeMsgParam.setEmpSexMsg("男");
            } else {
                employeeMsgParam.setEmpSexMsg("女");
            }
            // 判断并将职位状态代号转换为状态
            if (employee.getIsDelete() == 0) {
                employeeMsgParam.setIsDeleteMsg("在职");
            } else {
                employeeMsgParam.setIsDeleteMsg("离职");
            }
            employeeMsgParamList.add(employeeMsgParam);
        });
        // 赋值
        PageBeanParam<EmployeeMsgParam> pageBeanParam = PageBeanParam.<EmployeeMsgParam>builder()
                .current(current)
                .size(size)
                .count(employeeService.queryEmpCount(current, size, conditionList))
                .data(employeeMsgParamList)
                .build();
        return pageBeanParam;
    }

    /**
     * 根据员工号逻辑删除员工
     *
     * @param employeeId
     * @return
     */
    @Override
    public int deleteEmployee(String employeeId) {
        return employeeService.deleteEmployee(employeeId);
    }

    /**
     * 管理员修改除员工id以外的所有人的所有信息
     *
     * @return
     */
    @Override
    public int updateInfos(ManageParam manageParam) {
            // 对象转换
            Employee empDto = new Employee();
            BeanUtils.copyProperties(manageParam, empDto);

            return employeeService.updateInfos(empDto);

    }

    /**
     * 普通员工可以修改除了id，离职状态，工资，权限，入职时间，身份证和手机号以外的所有个人信息
     *
     * @param normalEmpParam
     * @return
     */
    @Override
    public int updateInfo(NormalEmpParam normalEmpParam) {

            // 对象转换
            Employee empDto = new Employee();
            BeanUtils.copyProperties(normalEmpParam, empDto);

            return employeeService.updateInfos(empDto);

    }

    /**
     * 根据员工id修改个人密码
     *
     * @param employeeId
     * @param password
     * @param request
     * @return
     */

    @Override
    public boolean updatePassord(String employeeId, String password, HttpServletRequest request) {
        if (employeeService.updatePassord(employeeId, password)) {
            String token = request.getHeader("token");
            if (redisUtils.hasKey(token)) {
                redisUtils.del(token);
            }
            return true;
        }
        return false;
    }
}
