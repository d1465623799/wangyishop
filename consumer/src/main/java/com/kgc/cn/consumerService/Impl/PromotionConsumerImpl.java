package com.kgc.cn.consumerService.Impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Lists;
import com.kgc.cn.commonService.GoodsConmonService;
import com.kgc.cn.commonService.PromotionConmonService;
import com.kgc.cn.consumerService.PromotionConsumerService;
import com.kgc.cn.model.PageBeanParam;
import com.kgc.cn.model.PromotionDataParam;
import com.kgc.cn.model.PromotionParam;
import com.kgc.cn.model.dto.Promotion;
import com.kgc.cn.utils.Date.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Ding on 2019/12/12.
 */
@Service
public class PromotionConsumerImpl implements PromotionConsumerService {
    @Reference
    private PromotionConmonService promotionConmonService;

    @Reference
    private GoodsConmonService goodsConmonService;

    /**
     * 修改促销活动信息
     *
     * @param promotionParam
     * @return
     * @throws Exception
     */
    @Override
    public String updatePromotion(PromotionParam promotionParam) throws Exception {
        Promotion promotion = new Promotion();
        BeanUtils.copyProperties(promotionParam, promotion);
        return promotionConmonService.updatePromotion(promotion);
    }

    /**
     * 精确查询活动,删除过期活动,
     * 根据实际需求将结束时间需加1
     */
    @Override
    public PromotionDataParam queryPromotion(String goodId) throws Exception {
        Promotion promotion = promotionConmonService.queryPromotion(goodId);
        if (promotion == null) {
            return null;
        }
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

        Date timeEnd = sf.parse(promotion.getTimeEnd());
        Date timeNow = new Date();
        //后端返回的促销活动信息
        PromotionDataParam promotionDataParam = PromotionDataParam.builder()
                .goodName(promotion.getGoodName())
                .discount(promotion.getDiscount())
                .timeStart(promotion.getTimeStart())
                .timeEnd(promotion.getTimeEnd())
                .build();

        //过期活动删除，不过期的向页面返回
        if (timeEnd.before(timeNow)) {
            promotionConmonService.deletePromotion(goodId);
            return null;
        } else {
            return promotionDataParam;
        }

    }

    /**
     * 查询所有未结束的促销活动并分页
     */
    @Override
    public PageBeanParam<PromotionDataParam> promotionPage(int current, int size) {

        //从数据库查询
        List<Promotion> promotionList = promotionConmonService.promotionPage(current, size);
        //判断从数据库拿来的活动信息
        if (CollectionUtils.isEmpty(promotionList)) {
            return null;
        } else {
            List<PromotionDataParam> promotionDataParamList = Lists.newArrayList();
            //把拿过来的活动信息一个个循环判断，没过期的传给页面，过期的不传
            promotionList.forEach(promotion -> {
                PromotionDataParam promotionDataParam = PromotionDataParam.builder().build();
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date timeEnd = sf.parse(promotion.getTimeEnd());
                    Date timeNow = new Date();
                    if (timeEnd.after(timeNow)) {
                        BeanUtils.copyProperties(promotion, promotionDataParam);
                        promotionDataParamList.add(promotionDataParam);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });
            PageBeanParam<PromotionDataParam> pageBeanParam = PageBeanParam.<PromotionDataParam>builder()
                    .current(current)
                    .size(size)
                    .count(queryCount())
                    .data(promotionDataParamList)
                    .build();
            return pageBeanParam;
        }
    }

    /**
     * 查询促销活动总数量
     * 用于分页
     *
     * @return
     */
    private int queryCount() {
        return promotionConmonService.queryCount();
    }


    /**
     * 增加促销活动
     *
     * @param promotionParam
     * @return
     */
    @Transactional
    @Override
    public String addPromotion(PromotionParam promotionParam) {

        // 判断日期格式
        if (isRight(promotionParam.getTimeStart()) && isRight(promotionParam.getTimeEnd())) {
            // 判断起止日期顺序
            if (promotionParam.getTimeEnd().compareTo(promotionParam.getTimeStart()) < 0) return "6";
            try {
                // 判断开始时间
                if (promotionParam.getTimeStart().compareTo(DateUtils.accurateDay(new Date())) <= 0) return "7";
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // 新建promotion类并将不会改变的值传进去
            Promotion promotionDto = new Promotion();
            promotionDto.setTimeStart(promotionParam.getTimeStart());
            promotionDto.setTimeEnd(promotionParam.getTimeEnd());
            promotionDto.setDiscount(promotionParam.getDiscount());
            // 接收传入的商品ID集合
            Set<String> set = promotionParam.getGoodIdSet();

            // 判断集合也就是传入的商品ID是否为空
            if (set.isEmpty() && set.size() == 0) return "4";

            StringBuffer outOfSell = new StringBuffer();

            // 循环遍历判断
            for (String goodId : set) {
                // 判断商品是否可以参加活动
                if (!goodsConmonService.hasGood(goodId) || promotionConmonService.isDelete(goodId)) {
                    outOfSell.append(goodId + ",");
                }else {
                    // 判断该商品有没有折扣，有就删除
                    if (promotionConmonService.hasPromorion(goodId)) {
                        promotionConmonService.deletePromotion(goodId);
                    }
                    promotionDto.setGoodId(goodId);
                    promotionConmonService.addPromotion(promotionDto);
                }
            }
            // 判断添加结果
            if (!"".equals(outOfSell.toString())) return "以下商品不可参加活动：" + outOfSell;

            return "成功~~~";
        }
        return "2";
    }

    /**
     * 判断字符串是不是符合yyyy-MM-dd
     *
     * @param str
     * @return
     */
    public boolean isRight(String str) {
        try {
            DateUtils.accurateDay(str);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
