package com.kgc.cn.consumerService;

import com.kgc.cn.model.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

/**
 * Created by Ding on 2019/12/12.
 */
public interface EmployeeConsumerService {
    //登录
    String login(String phone, String password, HttpServletRequest request) throws ParseException;

    // 注销登录
    void loginOut(HttpServletRequest request);

    //查询未出勤人员
    PageBeanParam<AttendanceParam> queryAbsenteeism(int current, int size);

    //查询id对应的权限
    String queryRoleName(int roleId);

    //通过excel批量添加员工
    String addEmployee(String filePath) throws Exception;

    // 根据工号姓名模糊条件查询，可用起止时间缩小查询范围并分页
    PageBeanParam<EmployeeMsgParam> queryEmployeeInfo(int current, int size, SearchEmployeeParam searchEmployeeParam);

    // 根据员工号删除员工（逻辑删除）
    int deleteEmployee(String employeeId);

    // 管理员修改信息
    int updateInfos(ManageParam manageParam);

    // 普通员工修改信息
    int updateInfo(NormalEmpParam normalEmpParam);

    // 修改个人密码
    boolean updatePassord(String employeeId, String password, HttpServletRequest request);
}
