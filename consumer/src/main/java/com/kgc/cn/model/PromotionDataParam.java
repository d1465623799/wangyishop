package com.kgc.cn.model;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiOperation(value = "促销model查询分页")
@Builder
public class PromotionDataParam implements Serializable {
    private static final long serialVersionUID = 7692195832557905447L;
    @ApiModelProperty(value = "商品名称")
    private String goodName;
    @ApiModelProperty(value = "开始时间")
    private String timeStart;
    @ApiModelProperty(value = "结束时间")
    private String timeEnd;
    @ApiModelProperty(value = "折扣力度：10为一折", example = "1")
    private Integer discount;

}
