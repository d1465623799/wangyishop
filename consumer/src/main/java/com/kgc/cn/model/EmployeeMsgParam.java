package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by boot on 2019/12/16
 */
@Data
@ApiModel(value = "员工信息显示model")
public class EmployeeMsgParam {
    @ApiModelProperty(value = "工号")
    private String empId;
    @ApiModelProperty(value = "姓名")
    private String empName;
    @ApiModelProperty(value = "年龄", example = "1")
    private Integer empAge;
    @ApiModelProperty(value = "邮箱")
    private String empEmail;
    @ApiModelProperty(value = "手机号")
    private String empPhone;
    @ApiModelProperty(value = "密码")
    private String empPassword;
    @ApiModelProperty(value = "地址")
    private String empAddress;
    @ApiModelProperty(value = "身份证")
    private String empIdentityCard;
    @ApiModelProperty(value = "入职时间")
    private String empTimeStart;
    @ApiModelProperty(value = "工资", example = "1")
    private Double empSalary;
    @ApiModelProperty(value = "部门名字")
    private String departName;
    @ApiModelProperty(value = "职位名")
    private String roleName;
    @ApiModelProperty(value = "性别信息")
    private String empSexMsg;
    @ApiModelProperty(value = "职工状态")
    private String isDeleteMsg;
}
