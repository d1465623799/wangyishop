package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by boot on 2019/12/19
 */
@Data
@ApiModel(value = "普通员工修改信息model")
public class NormalEmpParam {
    @ApiModelProperty(value = "工号")
    private String empId;
    @ApiModelProperty(value = "姓名")
    private String empName;
    @ApiModelProperty(value = "性别", example = "1")
    private Integer empSex;
    @ApiModelProperty(value = "年龄", example = "1")
    private Integer empAge;
    @ApiModelProperty(value = "邮箱")
    private String empEmail;
    @ApiModelProperty(value = "地址")
    private String empAddress;

}
