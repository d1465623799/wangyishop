package com.kgc.cn.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by boot on 2019/12/20
 */
@Data
@ApiModel(value = "管理员修改信息model")
public class ManageParam {
    @ApiModelProperty(value = "工号")
    private String empId;
    @ApiModelProperty(value = "姓名")
    private String empName;
    @ApiModelProperty(value = "性别", example = "1")
    private Integer empSex;
    @ApiModelProperty(value = "年龄", example = "1")
    private Integer empAge;
    @ApiModelProperty(value = "邮箱")
    private String empEmail;
    @ApiModelProperty(value = "手机号")
    private String empPhone;
    @ApiModelProperty(value = "地址")
    private String empAddress;
    @ApiModelProperty(value = "工资", example = "1")
    private Double empSalary;
    @ApiModelProperty(value = "部门id", example = "1")
    private Integer departId;
}
