package com.kgc.cn.utils.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * @auther zhouxinyu
 * @data 2019/12/13
 */
@Component
public class EmailUtil {
    @Autowired
    JavaMailSenderImpl mailSender;
    @Value("${spring.mail.username}")
    private String userName;

    /**
     * 发送带附件邮件
     *
     * @param receiver
     * @param subject
     * @param content
     * @param filePath
     * @throws MessagingException
     */
    @Async
    public void sendSimpleMail(String receiver, String subject, String content, String filePath) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
        messageHelper.setFrom(userName);//发件人
        messageHelper.setTo(receiver);//收信人
        messageHelper.setSubject(subject);//主题
        messageHelper.setText(content);//内容

        //带附件,加一个附件地址，加一个sendAttachments方法
        FileSystemResource file = new FileSystemResource(new File(filePath));
        String filename = filePath.substring(filePath.lastIndexOf(File.separator));
        messageHelper.addAttachment(filename, file);
        mailSender.send(message);
    }

    /**
     * 发送HTML邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容（可以包含<html>等标签）
     */
    @Async
    public void sendHtmlMail(String to, String subject, String content, String phone) {
        //使用MimeMessage，MIME协议
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        //MimeMessageHelper帮助我们设置更丰富的内容
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(userName);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(buildContent(content, phone), true);//true代表支持html
            // 向模板插入图片
            String alarmIconName = "img/success.png";
            ClassPathResource img = new ClassPathResource(alarmIconName);
            if (Objects.nonNull(img)) {
                helper.addInline("icon-alarm", img);
            }
            mailSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加入html模板
     *
     * @param name
     * @return
     * @throws IOException
     */

    public static String buildContent(String name, String phone) throws IOException {
        // 模板地址
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("emailModel/mail.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        // 将模板转化为流
        StringBuffer buffer = new StringBuffer();
        String line = "";
        try {
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (Exception e) {
            throw new RuntimeException("读取文件失败！");
        } finally {
            inputStream.close();
            bufferedReader.close();
        }
        // 生成日期
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH：mm:ss");
        String time = format.format(date);
        // 奖模板文本插入邮件
        String htmlText = MessageFormat.format(buffer.toString(), name, time, phone);
        return htmlText;
    }

}
