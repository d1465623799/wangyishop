package com.kgc.cn.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.kgc.cn.mapper")
public class MybatisConfig {
}
