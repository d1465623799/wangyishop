package com.kgc.cn.mapper;

import com.kgc.cn.model.dto.Employee;
import com.kgc.cn.model.dto.EmployeeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    long countByExample(EmployeeExample example);

    int deleteByExample(EmployeeExample example);

    int deleteByPrimaryKey(String empId);

    int insert(Employee record);

    int insertSelective(Employee record);

    List<Employee> selectByExample(EmployeeExample example);

    Employee selectByPrimaryKey(String empId);

    int updateByExampleSelective(@Param("record") Employee record, @Param("example") EmployeeExample example);

    int updateByExample(@Param("record") Employee record, @Param("example") EmployeeExample example);

    int updateByPrimaryKeySelective(Employee record);

    int updateByPrimaryKey(Employee record);

    //查询缺勤人员
    List<Employee> queryAbsenteeism(@Param("start") int start, @Param("size") int size);

    // 根据员工号和姓名模糊查询员工信息，可以用起止时间缩小范围
    List<Employee> queryEmployeeInfo(@Param("start") int start, @Param("size") int size, @Param("empId") String id,
                                     @Param("empName") String name, @Param("timeStart") String timeStart, @Param("timeEnd") String timeEnd);

    // 根据员工号逻辑删除员工
    int deleteEmployee(String employeeId);

    // 查询出勤表数据总数量
    int queryCount();

    // 查询职员总数
    int queryEmpCount(@Param("start") int start, @Param("size") int size, @Param("empId") String id,
                      @Param("empName") String name, @Param("timeStart") String timeStart, @Param("timeEnd") String timeEnd);

    // 根据身份证id修改isdelete
    int UpdateByIId(String empIdentityCard);

    // 根据员工id修改密码
    int UpdatePwdById(@Param("employeeId") String employeeId, @Param("password") String password);
}