package com.kgc.cn.providerServiceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.google.common.collect.Lists;
import com.kgc.cn.commonService.EmployeeConmonService;
import com.kgc.cn.mapper.EmployeeMapper;
import com.kgc.cn.mapper.RoleMapper;
import com.kgc.cn.model.dto.Employee;
import com.kgc.cn.model.dto.EmployeeExample;
import com.kgc.cn.model.dto.Role;
import com.kgc.cn.utils.DES.DESUtil;
import com.kgc.cn.utils.EmployeeUtil;
import com.kgc.cn.utils.activeMqUtil.ActiveMqUtil;
import com.kgc.cn.utils.email.EmailUtil;
import com.kgc.cn.utils.excel.EmployeeExcelModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.CollectionUtils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Ding on 2019/12/12.
 */
@Service
public class EmployeeProvicerImpl implements EmployeeConmonService {
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private RoleMapper roleMapper;

    /**
     * 通过角色id，查询对应的权限
     *
     * @param roleId
     * @return
     */
    @Override
    public String queryRoleName(int roleId) {
        Role role = roleMapper.selectByPrimaryKey(roleId);
        return role.getRoleName();
    }

    /**
     * 查询改手机号是否存在
     *
     * @param phone
     * @return
     */
    @Override
    public Employee queryEmployee(String phone) {
        EmployeeExample example = new EmployeeExample();
        example.createCriteria().andEmpPhoneEqualTo(phone);
        List<Employee> employeeList = employeeMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(employeeList)) {
            return null;
        }
        return employeeList.get(0);
    }

    /**
     * 通过身份证判断此人是否在库
     *
     * @param IdCard
     * @return
     */
    public Employee queryEmployeeByIdCard(String IdCard) {
        EmployeeExample example = new EmployeeExample();
        example.createCriteria().andEmpIdentityCardEqualTo(IdCard);
        List<Employee> employeeList = employeeMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(employeeList)) {
            return null;
        }
        return employeeList.get(0);
    }

    /**
     * 查询未出勤员工
     *
     * @return
     */
    @Override
    public List<Employee> queryAbsenteeism(int current, int size) {
        int start = (current - 1) * size;
        return employeeMapper.queryAbsenteeism(start, size);
    }


    @Autowired
    private EmployeeUtil employeeUtil;
    @Autowired
    private EmailUtil emailUtil;
    @Autowired
    private ActiveMqUtil activeMqUtil;

    /**
     * 通过excel表批量添加员工并发邮件通知
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    @Override
    @Async
    public String addEmployee(String filePath) throws Exception {
        // 读取excel表格
        Sheet sheet = new Sheet(1, 1, EmployeeExcelModel.class);
        InputStream inputStream = new FileInputStream(filePath);
        List<Object> list = EasyExcelFactory.read(inputStream, sheet);
        // 将读取出来的转类型
        List<EmployeeExcelModel> listVo = Lists.newArrayList();
        for (Object o : list) {
            listVo.add((EmployeeExcelModel) o);
        }
        String strold = "203重新入职者：";
        String strchongfu = "204添加员工中身份证号码重复，重复着姓名：";
        // 遍历，存sql
        for (EmployeeExcelModel employeeExcelModel : listVo) {
            Employee employee = new Employee();
            BeanUtils.copyProperties(employeeExcelModel, employee);
            Employee employeeold = queryEmployeeByIdCard(employee.getEmpIdentityCard());
            // 判断身份证号码是否存在(调用上面的方法)
            if (null == employeeold) {
                // 身份证号码不存在（即添加新员工）
                //自动生成工号（调用方法）
                String EmpId = employeeUtil.generateEmpId(employee.getEmpPhone(), employee.getEmpIdentityCard());
                employee.setEmpId(EmpId);
                //密码加密
                employee.setEmpPassword(DESUtil.getEncryptString(employee.getEmpPassword()));
                employee.setIsDelete(0);
                //加库
                employeeMapper.insert(employee);
                //将发邮件放入消息中间件
                activeMqUtil.sendEmailByActiveMq("sendemail", employee);
            } else {
                // 身份证号码重复（再判断是否以前入过职）
                if (employeeold.getIsDelete() == 1) {
                    // 以前入职过，但离职（即重新入职）
                    employeeMapper.UpdateByIId(employeeold.getEmpIdentityCard());
                    strold += employeeold.getEmpName() + ",";
                    //将发邮件放入消息中间件
                    activeMqUtil.sendEmailByActiveMq("sendemail", employee);
                } else {
                    // 以前未入职过，但是身份证号码重复（即身份证号码错误）
                    strchongfu += employee.getEmpName() + ",";
                }

            }
        }
        if ("203重新入职者：".equals(strold) && "204添加员工中身份证号码重复，重复着姓名：".equals(strchongfu)) {
            return "员工表添加成功";
        } else if ("203重新入职者：".equals(strold)) {
            return strchongfu;
        } else if ("204添加员工中身份证号码重复，重复着姓名：".equals(strchongfu)) {
            return strold;
        } else {
            return strchongfu + strold;
        }

    }

    /**
     * 监听发送邮件
     *
     * @param employee
     */
    @JmsListener(destination = "sendemail")
    private void sendEmailListener(Employee employee) {
        // 发邮件
        emailUtil.sendHtmlMail(employee.getEmpEmail(), "入职通知", employee.getEmpName(), employee.getEmpPhone());
    }

    /**
     * 通过id，name模糊查询，并可以通过起止时间缩小范围
     *
     * @param list
     * @return
     */
    @Override
    public List<Employee> queryEmployeeInfo(int current, int size, List<String> list) {
        int start = (current - 1) * size;
        return employeeMapper.queryEmployeeInfo(start, size, list.get(0), list.get(1), list.get(2), list.get(3));
    }

    /**
     * 根据员工号逻辑删除员工
     *
     * @param employeeId
     * @return
     */
    @Override
    public int deleteEmployee(String employeeId) {
        return employeeMapper.deleteEmployee(employeeId);
    }

    /**
     * 更新信息
     *
     * @param employee
     * @return
     */
    @Override
    public int updateInfos(Employee employee) {
        return employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 查询出勤表数据总数量
     * 用于分页
     *
     * @return
     */
    @Override
    public int queryCount() {
        return employeeMapper.queryCount();
    }

    /**
     * 查询职员总数
     *
     * @return
     */
    @Override
    public int queryEmpCount(int current, int size, List<String> list) {
        int start = (current - 1) * size;
        return employeeMapper.queryEmpCount(start, size, list.get(0), list.get(1), list.get(2), list.get(3));
    }

    /**
     * 根据员工id修改个人密码
     *
     * @param employeeId
     * @param password
     * @return
     */
    @Override
    public boolean updatePassord(String employeeId, String password) {
        String passwordDES = DESUtil.getEncryptString(password);
        if (employeeMapper.UpdatePwdById(employeeId, passwordDES) == 1) {
            return true;
        }
        return false;
    }

}
